# Generated by Django 4.1.3 on 2022-11-10 07:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('execusion', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shortenurl',
            name='course_description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='shortenurl',
            name='course_link',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='shortenurl',
            name='course_name',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='shortenurl',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='shortenurl',
            name='url',
            field=models.TextField(blank=True, null=True),
        ),
    ]
