from django.db import models
from django.template.defaultfilters import slugify
import string
import random
import pyshorteners


# Create your models here.
class DateTimeMixin(models.Model):
    create_at = models.DateTimeField(auto_now_add = True)
    update_at = models.DateTimeField(auto_now = True)

    class Abstract:
        True

class ShortenUrl(DateTimeMixin):
    course_name = models.CharField(max_length=500, blank=True, null=True)
    course_description = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    course_link = models.TextField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)


    def __str__(self) -> str:
        return self.course_name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.course_name)

        data = self.course_name[0:50]

        sorten_link = pyshorteners.Shortener()

        self.url = sorten_link.tinyurl.short(self.course_link)
        return super(ShortenUrl, self).save(*args, **kwargs)

class Creator(DateTimeMixin):
    name = models.CharField(max_length=50)

class Book(DateTimeMixin):
    name = models.CharField(max_length=100, null=True, blank=True)
    authors = models.ManyToManyField(Creator)

    def __str__(self) -> str:
        return self.name



