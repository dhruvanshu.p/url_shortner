from .models import *
from rest_framework import serializers


class UrlSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShortenUrl
        fields = ['course_name','course_description','course_link','url']
        read_only_field = ['url']

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Creator
        fields = ['id','name']
        read_only_fields = ['id'] 


class BookSerializer(serializers.ModelSerializer):

    authors = AuthorSerializer(many = True)

    class Meta:
        model = Book
        fields = ['id','name','authors']
        read_only_fields = ['id'] 
