from django.urls import path 
from .views import *

urlpatterns = [
    path('url/', url_sortner, name='urlsortner'),
    path('view_course/<slug>', view_page, name='view'),
    path('url_view/<pk>', url_response, name='url-response'),
    path('create_urlshort/',ShortnerCreate.as_view(), name='short-url')
]