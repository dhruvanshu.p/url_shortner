import string
from django.shortcuts import render
from .models import *
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.views import APIView
from .serializers import *
# Create your views here.
# from pyshorteners import Shortener
import random

def url_sortner(request):

    obj = ShortenUrl.objects.all()
   
    context = {
        'obj':obj
    }

    return render(request, 'index.html', context)



def view_page(request,slug):


    
    data = ShortenUrl.objects.get(slug = slug)

    context = {
        'obj':data
    }

    return render(request, 'view.html', context)

# class ShortenUrlCreateView(Ao)

@api_view(['GET'])
def url_response(request,pk):
    data = ShortenUrl.objects.get(pk = pk)


    url = str(data.url)
    

    return Response(url)


class ShortnerCreate(APIView):

    def post(self, request):
        serializer = UrlSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response("Saved Succesfully")
        return Response("Data Is Incorrect")        

    def get(self, request):

        model = ShortenUrl.objects.all()

        serializer = UrlSerializer(instance=model, many = True)
        print(serializer.data)

        return Response(serializer.data)





class AuthorViewSerializer(viewsets.ModelViewSet):
    queryset = Creator.objects.all()
    serializer_class = AuthorSerializer


class BookViewSerializer(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
        
